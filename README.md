# Indexbot Grimoire
## What is it?
Server software for index-bot allowing authorized users to sync their song databases with eachother. By connecting through a websocket, authorized users can update the
grimoire's song database and subsequently pull updates into their own. It's written in Java using a websocket api, along with gson for parsing json input and
picocli for parsing command line options. It uses an sqlite database, though this could be easily swapped out due to the similarities for a postgresql db or
mysql.
## Why so many dependencies?
I wanted to try new things.
## What are the commands?
### Console Commands

`exit - closes the db connection and shuts down the server gracefully`

`adduser - opens interface for adding a new user with a specified password`

`removeuser - opens interface for removing a specific user`

`logincheck - debug command to test a given username/password combination`

### Websocket Commands
``sync - tells the webserver you would like to sync with it. attempts to update the server song db
 with the contents of 'data' and then gets a response consisting of the entire song db``
 
 ## Json Format
 ### Web Command (client to server)
 ```json
{
  "data" : "some data in a string, probably an array of song objects",
  "command" : "some web command ie sync",
  "username" : "username for the grimoire server",
  "password" : "password for the given username"
}
```
### Server Response
```json
{
  "error" : 0,
  "data" : "some data or error message"
}
```
When receiving a response, the client should check the "error" field. If it's not 0, an error occurred, and the data field should contain some message or reason.
### Song
```json
{
  "url" : "some url to a song",
  "anime" : "the anime the song is from"
}
```