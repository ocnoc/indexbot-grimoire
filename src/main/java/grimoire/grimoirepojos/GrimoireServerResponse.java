package grimoire.grimoirepojos;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class GrimoireServerResponse{

	@SerializedName("data")
	private String data;

	@SerializedName("error")
	private int error;

	public void setData(String data){
		this.data = data;
	}

	public String getData(){
		return data;
	}

	public void setError(int error){
		this.error = error;
	}

	public int getError(){
		return error;
	}

	@Override
 	public String toString(){
		return 
			"GrimoireServerResponse{" + 
			"data = '" + data + '\'' + 
			",error = '" + error + '\'' + 
			"}";
		}
}