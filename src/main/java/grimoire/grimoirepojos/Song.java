package grimoire.grimoirepojos;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

import java.util.Objects;

@Generated("com.robohorse.robopojogenerator")
public class Song{

	@SerializedName("url")
	private String url;

	@SerializedName("anime")
	private String anime;

	public void setUrl(String url){
		this.url = url;
	}

	public String getUrl(){
		return url;
	}

	public void setAnime(String anime){
		this.anime = anime;
	}

	public String getAnime(){
		return anime;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Song song = (Song) o;
		return Objects.equals(getUrl(), song.getUrl()) &&
				Objects.equals(getAnime(), song.getAnime());
	}

	@Override
	public int hashCode() {
		return Objects.hash(getUrl(), getAnime());
	}

	@Override
 	public String toString(){
		return 
			"Song{" + 
			"url = '" + url + '\'' + 
			",anime = '" + anime + '\'' + 
			"}";
		}
}