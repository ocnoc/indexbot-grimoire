package grimoire;

import grimoire.grimoirepojos.Song;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.logging.Logger;

public class GrimoireLibrary {
    private static HashSet<Song> songs = new HashSet<>();
    private static Logger logger = Logger.getLogger(GrimoireLibrary.class.getName());

    public static boolean addSong(Song song) {
        return songs.add(song);
    }

    public static Collection<Song> getAllSongs() {
        return new ArrayList<>(songs);
    }

    public static void updateSong(String url, Song song) {
        for(Song s : songs) {
            if(s.getUrl().equals(url)) {
                if(s.getAnime().equals(song.getAnime())) {
                    // We already have them equal
                    return;
                }
                songs.remove(s);
                songs.add(song);
                return;
            }
        }
        addSong(song);
    }

    public static void saveSongsToDatabase() {
        List<Song> songsToSave = new ArrayList<>();
        List<Song> songsInDB = GrimoireDatabase.getAllSongs();
        for(Song s : songs) {
            if(songsInDB.contains(s)) {
                continue;
            }
            songsToSave.add(s);
        }
        logger.info(String.format("Saving %d songs to database out of %d loaded...", songsToSave.size(), songs.size()));
        GrimoireDatabase.updateMultipleURLs(songsToSave);
        logger.info(String.format("Saved %d songs to database from library.", songsToSave.size()));
    }

    public static void loadSongsFromDatabase() {
        List<Song> dbSongs = GrimoireDatabase.getAllSongs();
        songs.addAll(dbSongs);
        logger.info(String.format("Loaded %d songs from database into library.", dbSongs.size()));
    }

}
