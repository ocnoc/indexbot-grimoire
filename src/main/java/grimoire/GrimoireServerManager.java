package grimoire;

import picocli.CommandLine;

import java.io.*;
import java.net.InetSocketAddress;
import java.net.URL;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.sql.SQLException;
import java.util.concurrent.*;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;

@CommandLine.Command(header =
        "@|fg(green)  _____          _             _____      _                 _          \n" +
                "|_   _|        | |           |  __ \\    (_)               (_)         \n" +
                "  | | _ __   __| | _____  __ | |  \\/_ __ _ _ __ ___   ___  _ _ __ ___ \n" +
                "  | || '_ \\ / _` |/ _ \\ \\/ / | | __| '__| | '_ ` _ \\ / _ \\| | '__/ _ \\\n" +
                " _| || | | | (_| |  __/>  <  | |_\\ \\ |  | | | | | | | (_) | | | |  __/\n" +
                " \\___/_| |_|\\__,_|\\___/_/\\_\\  \\____/_|  |_|_| |_| |_|\\___/|_|_|  \\___|\n" +
                "                                                                      \n" +
                "                                                                      |@",
        description = "Handles the running of the grimoire server", name = "IndexGrimoire", mixinStandardHelpOptions = true, version = "IndexGrimoire 1.0")
public class GrimoireServerManager implements Callable {
    @CommandLine.Option(names = {"-p", "--port"}, required = true, paramLabel = "PORT", description = "the port to listen on")
    int port;
    @CommandLine.Option(names = {"-H", "--hostname"}, required = false, paramLabel = "ADDRESS", description = "host name to listen on")
    String hostname;

    public static final String ASCII_ART =
            " _____          _             _____      _                 _          \n" +
                    "|_   _|        | |           |  __ \\    (_)               (_)         \n" +
                    "  | | _ __   __| | _____  __ | |  \\/_ __ _ _ __ ___   ___  _ _ __ ___ \n" +
                    "  | || '_ \\ / _` |/ _ \\ \\/ / | | __| '__| | '_ ` _ \\ / _ \\| | '__/ _ \\\n" +
                    " _| || | | | (_| |  __/>  <  | |_\\ \\ |  | | | | | | | (_) | | | |  __/\n" +
                    " \\___/_| |_|\\__,_|\\___/_/\\_\\  \\____/_|  |_|_| |_| |_|\\___/|_|_|  \\___|\n" +
                    "                                                                      \n" +
                    "                                                                      \n";

    static Logger logger = Logger.getLogger(GrimoireServerManager.class.getName());
    private static final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
    private static ScheduledFuture<?> librarySaveHandler;

    GrimoireServer server;

    public GrimoireServer buildServer() {
        if (hostname != null) {
            InetSocketAddress address = new InetSocketAddress(hostname, 6666);
            if (address.isUnresolved()) {
                logger.log(Level.SEVERE, String.format("Failed to resolve hostname %s on port %d", hostname, port));
                System.exit(1);
            }
            return new GrimoireServer(address);
        }
        return new GrimoireServer(port);
    }

    public static void main(String[] args) {
        CommandLine.call(new GrimoireServerManager(), args);

    }

    public void acceptCommands() throws IOException, InterruptedException, SQLException, InvalidKeySpecException, NoSuchAlgorithmException {
        boolean shutdown = false;
        BufferedReader sysin = new BufferedReader(new InputStreamReader(System.in));
        BufferedWriter sysout = new BufferedWriter((new OutputStreamWriter(System.out)));
        while (!shutdown) {
            String command = sysin.readLine();
            switch (command) {
                case "exit":
                    closeServer();
                    shutdown = true;
                    break;
                case "adduser":
                    sysout.write("Username: ");
                    sysout.flush();
                    String username = sysin.readLine();
                    sysout.write("Password: ");
                    sysout.flush();
                    String password = sysin.readLine();
                    if (Security.registerUser(username, password)) {
                        sysout.write("User added successfully\n");
                    } else {
                        sysout.write("Failed to add user\n");
                    }
                    sysout.flush();
                    break;
                case "removeuser":
                    sysout.write("Username: ");
                    sysout.flush();
                    String user = sysin.readLine();
                    sysout.write("Are you sure? (Y/n): ");
                    sysout.flush();
                    char choice = sysin.readLine().charAt(0);
                    if (choice != 'Y') {
                        break;
                    }
                    if (Security.deleteUser(user)) {
                        sysout.write("User deleted successfully.\n");
                    } else {
                        sysout.write("An error occured. Does that user exist?\n");
                    }
                    sysout.flush();
                    break;
                case "logincheck":
                    sysout.write("Username: ");
                    sysout.flush();
                    String usern = sysin.readLine();
                    sysout.write("Password: ");
                    sysout.flush();
                    String pass = sysin.readLine();
                    sysout.write(Security.checkUserLogin(usern, pass) ? "Success!\n" : "Failed\n");
                    sysout.flush();
                    break;
                default:
                    sysout.write(String.format("Command \"%s\" not found\n", command));
                    sysout.flush();
            }
        }
    }

    public void closeServer() throws InterruptedException, SQLException {
        logger.info("Server shutting down.");
        server.stop(1);
        GrimoireLibrary.saveSongsToDatabase();
        librarySaveHandler.cancel(true);
        scheduler.shutdown();
        GrimoireDatabase.disconnect();
    }

    @Override
    public Object call() throws Exception {
        System.out.println(ASCII_ART);
        loadLoggingConfig();
        server = buildServer();
        if (server == null) {
            System.out.println("Server is null, something did not initialize right");
            System.exit(1);
        }
        server.start();
        GrimoireDatabase.connect();
        GrimoireLibrary.loadSongsFromDatabase();
        setupLibrarySave();
        logger.info(String.format("Server started on port %d", port));
        acceptCommands();
        return null;
    }

    public static void setupLibrarySave() {
        final int SAVE_TIME_IN_SECONDS = 300; // 5 minutes
        final Runnable saveDB = GrimoireLibrary::saveSongsToDatabase;
        librarySaveHandler = scheduler.scheduleAtFixedRate(saveDB, SAVE_TIME_IN_SECONDS, SAVE_TIME_IN_SECONDS, TimeUnit.SECONDS);
    }

    public static void loadLoggingConfig() {
        URL url = GrimoireServerManager.class.getClassLoader().getResource("logging.properties");
        try {
            FileInputStream config = new FileInputStream(url.getPath());
            LogManager.getLogManager().readConfiguration(config);
        } catch (NullPointerException | IOException e) {
            logger.log(Level.WARNING, "Failed to load logging config, using default.");
        }
    }
}
