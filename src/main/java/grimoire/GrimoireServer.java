package grimoire;

import org.java_websocket.WebSocket;
import org.java_websocket.drafts.Draft;
import org.java_websocket.exceptions.InvalidDataException;
import org.java_websocket.handshake.ClientHandshake;
import org.java_websocket.handshake.ServerHandshakeBuilder;
import org.java_websocket.server.WebSocketServer;

import java.net.InetSocketAddress;
import java.net.UnknownHostException;
import java.util.logging.Level;
import java.util.logging.Logger;


public class GrimoireServer extends WebSocketServer {
    private static Logger logger = Logger.getLogger(GrimoireServer.class.getName());

    public GrimoireServer(int port) {
        super(new InetSocketAddress(port));
    }

    public GrimoireServer(InetSocketAddress address) {
        super(address);
    }

    @Override
    public void onOpen(WebSocket conn, ClientHandshake handshake) {
        logger.log(Level.INFO, String.format("Client from %s connected.", conn.getRemoteSocketAddress().getAddress().getHostName()));
    }

    @Override
    public void onClose(WebSocket conn, int code, String reason, boolean remote) {
        logger.log(Level.INFO, String.format("Client from %s closed connection with reason %s", conn.getRemoteSocketAddress().getAddress().getHostAddress(), reason));
    }

    @Override
    public void onMessage(WebSocket conn, String message) {
        GrimoireMessageHandler.handleMessage(conn, message);
    }

    @Override
    public void onError(WebSocket conn, Exception ex) {

    }

    @Override
    public void onStart() {

    }
}
